"""
cheatsheet fastapi
author : aaljabbar.dev
email: aaljabbar.dev@gmail.com
date : 2021-09-06 (yyyy-mm-dd)

run this app using uvicorn with command:
"uvicorn main:app --reload"

"""

from fastapi import FastAPI
from typing import Optional
# from Typing import Optional

app = FastAPI()

# define index  end point
@app.get('/')
def index():
	ret = {'data':{'success':True, 'message':'Welcomes to fastApi'}}
	return ret

# define blog end point
@app.get('/blog')
def index():
	ret = {'data':{'success':True, 'message':'Welcomes to blog'}}
	return ret

# define blog end point with parameter
# by default, all parameters treat as string

@app.get('/blog/{id}')
def index(id: int):
	ret = {'data':{'success':True, 'message':'Welcomes to blog '}, 'id': id}
	return ret